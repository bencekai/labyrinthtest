package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LabyrinthImpl implements Labyrinth {

    public int rows;
    public int columns;
    private CellType[][] labyrinth;
    private Coordinate PlayerPosition;
    private boolean isFinished;

    public LabyrinthImpl() {
    }

@Override
        public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());
            setSize(width, height);

            this.labyrinth = new CellType[height][width];

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            labyrinth[ww][hh] = CellType.WALL;
                            break;
                        case 'E':
                            labyrinth[ww][hh] = CellType.END;
                            break;
                        case 'S':
                            labyrinth[ww][hh] = CellType.START;
                            setPlayerPosition(ww, hh);
                            break;
                        default:
                            labyrinth[ww][hh] = CellType.EMPTY;
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
        public int getWidth() {
        if (rows == 0) {
            return -1;
        } else {
            return rows;
        }
    }

    @Override
        public int getHeight() {
        if (columns == 0) {
            return -1;
        } else {
            return columns;
        }
    }

    @Override
        public CellType getCellType(Coordinate c) throws CellException {
        if (c.getCol() < 0 || c.getCol() >= labyrinth.length || c.getRow() < 0 || c.getRow() >= labyrinth[0].length) {
            throw new CellException(c.getCol(), c.getRow(), "Nincs cella a megadott helyen!");
        }
        return labyrinth[c.getCol()][c.getRow()];
    }

    @Override
        public void setSize(int width, int height) {
        rows = width;
        columns = height;
        CellType[][] c = new CellType[height][width];
        labyrinth = c;
    }

    @Override
        public void setCellType(Coordinate c, CellType type) throws CellException {
        try {
            if (c.getCol() < 0 || c.getCol() >= labyrinth.length || c.getRow() < 0 || c.getRow() >= labyrinth[0].length) {
                throw new CellException(c.getRow(), c.getCol(), "Nincs semmi a megadott helyen!");
            } else {
                labyrinth[c.getCol()][c.getRow()] = type;
                if (type == CellType.START) {
                    labyrinth[PlayerPosition.getCol()][PlayerPosition.getRow()] = labyrinth[c.getCol()][c.getRow()];
                }
            }
        } catch (NullPointerException e) {
        }
    }

    @Override
        public Coordinate getPlayerPosition() {
        return PlayerPosition;
    }

    @Override
        public boolean hasPlayerFinished() {
        if (labyrinth[PlayerPosition.getCol()][PlayerPosition.getRow()] == CellType.END) {
            return true;
        } else {
            return isFinished;
        }
    }

    @Override
        public List<Direction> possibleMoves() {
        List<Direction> directions = new ArrayList<>();
        if (labyrinth[PlayerPosition.getCol()][PlayerPosition.getRow() + 1] == CellType.EMPTY) {
            directions.add(Direction.SOUTH);
        }
        if (labyrinth[PlayerPosition.getCol()][PlayerPosition.getRow() - 1] == CellType.EMPTY) {
            directions.add(Direction.NORTH);
        }
        if (labyrinth[PlayerPosition.getCol() + 1][PlayerPosition.getRow()] == CellType.EMPTY) {
            directions.add(Direction.EAST);
        }
        if (labyrinth[PlayerPosition.getCol() - 1][PlayerPosition.getRow() + 1] == CellType.EMPTY) {
            directions.add(Direction.WEST);
        }
        return directions;
    }

    @Override
        public void movePlayer(Direction direction) throws InvalidMoveException {
        if (null != direction) {
            switch (direction) {
                case EAST:
                    setPlayerPosition(PlayerPosition.getCol(), PlayerPosition.getRow() + 1);
                    break;
                case WEST:
                    setPlayerPosition(PlayerPosition.getCol(), PlayerPosition.getRow() - 1);
                    break;
                case SOUTH:
                    setPlayerPosition(PlayerPosition.getCol() - 1, PlayerPosition.getRow());
                    break;
                case NORTH:
                    setPlayerPosition(PlayerPosition.getCol() + 1, PlayerPosition.getRow());
                    break;
                default:
                    setPlayerPosition(PlayerPosition.getCol(), PlayerPosition.getRow());
                    break;
            }
        }
    }

    public void setPlayerPosition(int column, int row) {
        Coordinate c = new Coordinate(column, row);
        PlayerPosition = c;
    }
}
